<?php

/**
 * Этот интерфейс изменять нельзя
 */
interface DividerInterface
{
    public function divide($a, $b);
}

/**
 * Этот класс Изменять нельзя
 */
class VendorDivider implements DividerInterface
{
    public function divide($a, $b)
    {
        echo $a / $b;
    }
}

/**
 * Этот класс Изменять нельзя
 */
final class VendorProgram
{
    private $divider;

    public function __construct(DividerInterface $divider)
    {
        if (!$divider instanceof DividerInterface) {
            throw new Exception('$divider does not implement DividerInterface');
        }

        $this->divider = $divider;
    }

    public function divide()
    {
        $a = 3;
        $b = 0;

        $this->divider->divide($a, $b);
    }
}



//todo Задача: Планировалось использовать VendorDivider в программе VendorProgram но оказалось что VendorDivider при деление на 0 возвращает ошибку
//todo Сделайте так чтобы при деление на 0 возвращался 0 во всех остальных случаях a / b
//todo Подсказка используйте наследование чтобы переопределить логику VendorDivider
//====================== пишите код тут

class MyDivider
{
    //todo тут можно писать код
}

//todo тут можно подменить клас на MyDivider нужно подумать как
$divider = new VendorDivider();

//====================== пишите код тут


/**
 * Этот код изменять нельзя
 */
$program = new VendorProgram($divider);
$program->divide();